package controller;

import java.util.ArrayList;

import model.data_structures.ArregloDinamico;

import java.util.Iterator;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import model.logic.MVCModelo;
import model.logic.Maps;
import model.logic.NodoCoordenadas;
import model.logic.NodoVial;
import model.logic.TravelTime;
import model.logic.Zona;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	private final static int N=20;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.cargarTXT();
				modelo.cargarTXT2();
				System.out.println("Se ha cargado correctamente la informacion de vertices y arcos");
				System.out.println("El numero de vertices fue de: " + modelo.darVertices());
				System.out.println("El numero de arcos fue de: " + modelo.darArcos());
				break;

			case 2:
				int cc = modelo.darGrafo().cc();
				System.out.println("El numero de CC fue de: " + cc);
				break;
			case 3:
				modelo.crearJSON();
				System.out.println("Se ha creado correctamente el archivo JSON con la informacion del grafo");
				break;
			case 4:
				modelo.cargarJSON();
				System.out.println("Se ha cargado correctamente el archivo JSON");
				System.out.println("El numero de vertices es de: " + modelo.darVertices());
				System.out.println("El numero de arcos es de: " + modelo.darArcos());
				break;
			case 5:
				Maps mapa = new Maps(modelo);
				System.out.println("Se cargo el mapa con la informacion de vertices y arcos");
				mapa.initFrame("Mapa Bogota");
				break;
			default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
