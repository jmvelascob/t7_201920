package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar información");
			System.out.println("2. Obtener el numero de Componentes Conectadas");
			System.out.println("3. Crear un archivo JSON con la informacion del grafo");
			System.out.println("4. Cargar el archivo JSON con la informacion del grafo");
			System.out.println("5. Cargar un mapa de Bogota con la informacion de los vertices y arcos");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
