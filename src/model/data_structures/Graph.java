package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class Graph <K extends Comparable<K>,V extends Comparable<V>>{

	
	public int V;
	public int E;
	private int CC;
	public HashLinearProbing<K,V> hash;
	public Cola<Arco> arcos;
	
	public Graph(int v)
	{
		hash=new HashLinearProbing<K,V>(v);
		CC = 0;
	}
	
	public boolean tieneV(K k)
	{
		return hash.contains(k);
	}
	
	public int darV()
	{
		return V;
	}
	
	public int darE()
	{
		return E;
	}
	
	public HashLinearProbing<K,V> darHash()
	{
		return hash;
	}
	
	public void addEdge(K idVertexIni, K idVertexFin, double cost)
	{
		if(!tieneV(idVertexIni)|| !tieneV(idVertexFin))
		{
			System.out.println("No hay vertice");
		}
		else{
			Vertice ini = (Vertice)hash.get(idVertexIni);
			Vertice fin = (Vertice)hash.get(idVertexFin);
			Arco i = new Arco(cost, ini, fin);
			Arco f = new Arco(cost, fin, ini);
			ini.agregarArco(i);
			fin.agregarArco(f);
			E++;
		}
		
	}
	
	public V getInfoVertex(K idVertex)
	{
		if(!hash.contains((idVertex)))
			return null;
		else
		{
			Vertice v=(Vertice) hash.get(idVertex);
			V p=(V) v.darInfo();
			return p;
		}
		
}
	
	public void setInfoVertex(K idVertex, V infoVertex)
	{
		if(!hash.contains(idVertex))
			System.out.println("No existe");
		else
		{
			Vertice v=(Vertice) hash.get(idVertex);
			v.cambiarInfo((InfoVer)infoVertex);
		}
	}
	
	public double getCostArc(K idVertexIni, K idVertexFin)
	{
		int cont=0;
		Node<Arco> a=arcos.darPrimer();
		double cc=-1;
		while(cont<arcos.darTama�o())
		{
			if((int)a.darData().darinicial().darid()==((int)idVertexIni)&&(int)a.darData().darfinal().darid()==((int)idVertexFin))
			{
				cc= a.darData().darCosto();
			}
				a=a.darSiguiente();
			cont++;
		}
		return cc;
	}
	
	public void setCostArc(K idVertexIni, K idVertexFin, double cost)
	{
		int cont=0;
		Node<Arco> a=arcos.darPrimer();
		while(cont<arcos.darTama�o())
		{
			if((int)a.darData().darinicial().darid()==((int)idVertexIni)&&(int)a.darData().darfinal().darid()==((int)idVertexFin))
			{
				a.darData().cambiarCosto(cost);
			}
				a=a.darSiguiente();
				cont++;
		}
	}
	
	public void addVertex(K idVertex, V infoVertex)
	{
		Vertice ver = new Vertice(idVertex, (InfoVer)infoVertex);
		V x = (V) ver;
		hash.putInSet(idVertex, x);
		V++;
	}
	
	public Iterable <K> adj (K idVertex)
	{
		Vertice ver= (Vertice)hash.get( idVertex);
		return (Iterable<K>) ver.iterator();
	}
	
	public void uncheck()
	{
		Iterator<K> llaves=(Iterator<K>) hash.keys();
		while(llaves.hasNext())
		{
			K llave=llaves.next();
			Vertice v=(Vertice)hash.get(llave);
			v.unsetVisitado();
		}
	}
	 
	public void dfs(K s)
	{
		Vertice vertice = (Vertice)hash.get(s);
		Iterator<Arco> iter = vertice.iterator();
		if(vertice.esVisitado()==true)
		{
			return;
		}
		else
		{
			vertice.setVisitado(CC);
			while(iter.hasNext())
			{
				Arco arc = iter.next();
				K id = (K)arc.darfinal().darid();
				dfs(id);
			}
		}
	}
	
	public int cc()
	{
		CC = 0;
		uncheck();
		Iterator keys = hash.keys();
		while(keys.hasNext())
		{
			K key = (K) keys.next();
			Vertice vert = (Vertice)hash.get(key);
			if(vert.esVisitado()==false)
			{
				CC++;
			}
			dfs(key);
		}
		return CC;
	}
	
	public Iterator<K> getCC(K idVertex)
	{
		ArrayList<Vertice> arr = new ArrayList<Vertice>();
		cc();
		Vertice ini = (Vertice)hash.get(idVertex);
		ArrayList<Integer> arr2 = ini.darCC();
		if(arr2.size()==0)
		{
			return (Iterator<K>) arr.iterator();
		}
		else
		{
			int ccIni = arr2.get(0);
			Iterator keys = hash.keys();
			while(keys.hasNext())
			{
				K key = (K) keys.next();
				Vertice vert = (Vertice)hash.get(key);
				ArrayList<Integer> ccVer = vert.darCC();
				if(ccVer.size()!=0 && ccVer.get(0) == ccIni)
				{
					arr.add(vert);
				}
			}
			return (Iterator<K>) arr.iterator();
		}
	}
	
}
	