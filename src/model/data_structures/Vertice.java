package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class Vertice <K extends Comparable<K>> implements Comparable
{

	private ArrayList<Integer> visitado;
	private ArrayList<Arco> vecinos;
	private InfoVer info;
	private K id;
	
	public Vertice(K idd, InfoVer i)
	{
		visitado=new ArrayList<Integer>();
		vecinos=new ArrayList<Arco>();
		id=idd;
		info=i;
	}
	
	public K darid()
	{
	return id;
	}
	public InfoVer darInfo()
	{
		return info;
	}
	
	public void cambiarInfo(InfoVer lo)
	{
		info=lo;
	}
	public boolean esVisitado()
	{
		if(visitado.size()==0)
		{
			return false;
		}
		return true;
	}
	
	public void agregarArco(Arco v)
	{
		vecinos.add(v);
	}
	
	public boolean esVecino(Arco v)
	{
		return vecinos.contains(v);
	}
	
	public int numVEC()
	{
		return vecinos.size();
	}
	
	public void eliminarVecino(Arco v)
	{
		vecinos.remove(v);
	}
	
	public ArrayList<Integer> darCC()
	{
		return visitado;
	}
	
	public void setVisitado(int x)
	{
		visitado.add(x);
	}
	public void unsetVisitado()
	{
		for(int i=0;i<visitado.size();i++)
		{
			visitado.remove(i);
		}
	}
	
	public Iterator<Arco> iterator() 
	{
		// TODO Auto-generated method stub
		return vecinos.iterator();
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}


}
