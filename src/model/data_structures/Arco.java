package model.data_structures;

public class Arco implements Comparable<Arco> {
	
		private double costo;
		private transient Vertice inicial;
		private transient Vertice finall;
		
		public Arco(double pCosto, Vertice pInicial, Vertice pFinal)
		{
			costo=pCosto;
			inicial=pInicial;
			finall=pFinal;
		}
		
		public Vertice darinicial()
		{
			return inicial;
		}
		
		public Vertice darfinal()
		{
			return finall;
		}
		
		public double darCosto()
		{
			return costo;
		}
		public void cambiarCosto(double cost)
		{
			costo=cost;
		}

		@Override
		public int compareTo(Arco o) {
			// TODO Auto-generated method stub
			return 0;
		}
}
