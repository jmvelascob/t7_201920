package model.logic;


public class NodoCoordenadas implements Comparable<NodoCoordenadas>
{
	private double longitud;
	
	private double latitud;
	
	private NodoCoordenadas siguiente;
	
	private String zona;
	
	public NodoCoordenadas()
	{
		longitud = 0;
		latitud = 0;
	}
	
	public double darLongitud()
	{
		return longitud;
	}
	
	public double darLatitud()
	{
		return latitud;
	}
	
	public void cambiarLongitud( double pLongitud)
	{
		longitud = pLongitud;
	}
	
	public void cambiarLatitud( double pLatitud)
	{
		latitud = pLatitud;
	}
	
	public void cambiarSiguiente(NodoCoordenadas sig)
	{
		siguiente = sig;
	}
	
	public NodoCoordenadas darSiguiente()
	{
		return siguiente;
	}
	public void cambiarZona(String zon)
	{
		zona = zon;
	}
	
	public String darZona()
	{
		return zona;
	}

	@Override
	public int compareTo(NodoCoordenadas o) {
		// TODO Auto-generated method stub
		if(latitud-o.darLatitud()>0)
			return 1;
		else if(latitud==o.darLatitud())
			return 0;
		else 
			return -1;
	}

}


