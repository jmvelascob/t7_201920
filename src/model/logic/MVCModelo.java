package model.logic;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.Graph;
import model.data_structures.InfoVer;
import model.data_structures.Vertice;

/**
 * Definicion del modelo del mundo
 * @param <K>
 * @param <K>
 * @param <K>
 *
 */
public class MVCModelo<K> {
	
	private Zona zona;
	private int contadorZonas;
	private String instruccion;
	private ArregloDinamico<Zona> arr1;
	private ArregloDinamico<NodoVial> txt;
	private Graph grafo;
	private String path;
	int mayorSource;
	private ArrayList<Integer> arr;
	private ArrayList<Double> a;
	
	public MVCModelo()
	{
		txt=new ArregloDinamico<NodoVial>(10000);
		arr1= new  ArregloDinamico<Zona>(10000);
		grafo = new Graph(1000);
		arr = new ArrayList<Integer>();
		a = new ArrayList<Double>();
	}
	
	public void cargarTXT()
	{
		int numt=0;
		CSVReader reader = null;
		try
		{
			reader = new CSVReader(new FileReader("./data/bogota_vertices.txt"));
			try
			{
				String[] x =reader.readNext();
			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}
			for(String[] nextLine : reader) 
			{
				String[] ar = nextLine[0].split(";");
				InfoVer info = new InfoVer(Integer.parseInt(ar[0]), Double.parseDouble(ar[1]), Double.parseDouble(ar[2]), Integer.parseInt(ar[3]));
				Vertice ver = new Vertice(Integer.parseInt(ar[0]), info);
				grafo.addVertex(Integer.parseInt(ar[0]), info);				
				numt++;
			}
		}
		
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null  ) 
			{
				try 
				{
					reader.close();


				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}

	public void cargarTXT2()
	{
		int numt=0;
		CSVReader reader = null;
		try
		{
			reader = new CSVReader(new FileReader("./data/bogota_arcos.txt"));
			for(String[] nextLine : reader) 
			{
				String[] ar = nextLine[0].split(" ");
				for(int i=1;i<ar.length;i++)
				{
					int ini = Integer.parseInt(ar[0]);
					int fin = Integer.parseInt(ar[i]);
					if(grafo.tieneV(ini) && grafo.tieneV(fin))
					{
						InfoVer infoIni = (InfoVer) grafo.getInfoVertex(ini);
						InfoVer infoFin = (InfoVer) grafo.getInfoVertex(fin);
						double dist = distancia(infoIni.darLat(), infoIni.darLon(), infoFin.darLat(), infoFin.darLon());
						grafo.addEdge(ini, fin, dist);
					}
					
				}
				numt++;
			}
		}
		
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null  ) 
			{
				try 
				{
					reader.close();


				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public static double distancia(double startLat, double startLong, double endLat, double endLong) 
	{

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return 6371 * c;
	}

	public static double haversin(double val) 
	{
		return Math.pow(Math.sin(val / 2), 2);
	}
	
	public void crearJSON()
	{
		Gson gson = new Gson();
		try
		{
			FileWriter fileW = new FileWriter("./data/grafoJSON.json");
			BufferedWriter bufferedW = new BufferedWriter(fileW);
			bufferedW.write(gson.toJson(grafo));
			bufferedW.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al crear el archivo JSON- " + e.getMessage());
		}
	}
	
	public void cargarJSON()
	{
		try
		{
			JsonParser parser = new JsonParser();
			String path = "./data/grafoJSON.json";
	        FileReader fr = new FileReader(path);
	        JsonElement datos = parser.parse(fr);
	        String x = datos.toString();
	        Gson gson = new Gson();
	        grafo = gson.fromJson(x, Graph.class);
		}
		catch(Exception e)
		{
			System.out.println("No se pudo leer el json");
		} 
	}
	
	public Graph darGrafo()
	{
		return grafo;
	}

	public int darVertices()
	{
		return grafo.darV();
	}
	
	public int darArcos()
	{
		return grafo.darE();
	}	
	
}
	
	
	

